# LeadScore App API documentation

Documentation for implementing LeadScore API.

[JavaScript SDK Documentation](SDK.md#markdown-header-leadscore-app-javascript-sdk-documentation)

## Authentication
The API does all the authentication in the header of the request.
```
api-key: API_KEY
```

## Code Samples
* [PHP](samples/PHP.md#markdown-header-php-code-sample)

## Resources

### Stage
Endpoint: `https://www.leadscoreapp.dk/2.0/stages.json`

Accepts: `GET`

Description: Get list of stages

Example response:
```
{
    "stages": [
        {
            "id": "42",
            "name": "New Lead"
        },
        {
            "id": "43",
            "name": "Customer"
        }
    ]
}
```

### Owner
Endpoint: `https://www.leadscoreapp.dk/2.0/owners.json`

Accepts: `GET`

Description: Get list of owners

Example response:
```
{
    "owners": [
        {
            "id": "132",
            "full_name": "John Doe"
        },
        {
            "id": "133",
            "full_name": "Jane Doe"
        }
    ]
}
```

### Segments
Endpoint: `https://www.leadscoreapp.dk/2.0/segments.json`

Accepts: `GET`

Description: Get list of segments

Example response:
```
{
    "segments": [
        {
            "id": "13",
            "name": "Website form"
        },
        {
            "id": "14",
            "name": "Import"
        }
    ]
}
```

### Lead fields
Endpoint: `https://www.leadscoreapp.dk/2.0/lead/fields.json`

Accepts: `GET`

Description: Get list of lead fields

Example response:
```
{
    "fields": [
        {
            "slug": "email",
            "name": "Email address",
            "type": "lead"
        },
        {
            "slug": "full_name",
            "name": "Full name",
            "type": "lead"
        },
        {
            "slug": "first_name",
            "name": "First name",
            "type": "lead"
        },
        {
            "slug": "last_name",
            "name": "Last name",
            "type": "lead"
        },
        {
            "slug": "title",
            "name": "Title",
            "type": "lead"
        },
        {
            "slug": "company",
            "name": "Company",
            "type": "lead"
        },
        {
            "slug": "address",
            "name": "Address",
            "type": "lead"
        },
        {
            "slug": "zip",
            "name": "Zip code",
            "type": "lead"
        },
        {
            "slug": "city",
            "name": "City",
            "type": "lead"
        },
        {
            "slug": "country",
            "name": "Country",
            "type": "lead"
        },
        {
            "slug": "phone",
            "name": "Phone number",
            "type": "lead"
        },
        {
            "slug" => "phone_country_code",
            "name" => "Phone country code",
            "type" => "lead"
        },
        {
            "slug": "description",
            "name": "Description",
            "type": "lead"
        },
        {
            "slug": "42",
            "name": "Budget",
            "type": "customfield"
        }
    ]
}
```

### Lead
Endpoint: `https://www.leadscoreapp.dk/2.0/lead.json`

Accepts: `POST`

Description: 
- Add new lead, edit the lead if it already exists. 
- For creation `email` is required.
- For updating either `email` or `key` is required.

Example request:
```
'fields' => [
    'email'              => 'name@example.com', // Email address
    'key'                => '01234567890abcdf', // Lead's key
    'full_name'          => 'John Doe',         // Full name (Converts the name into first and last name)
    'first_name'         => 'John',             // First name
    'last_name'          => 'Doe',              // Last name
    'gender'             => 'MALE',             // Gender ('FEMALE', 'MALE' OR 'UNKNOWN')
    'title'              => 'Customer',         // Title
    'company'            => 'LeadScore App',    // Company
    'address'            => 'Bredgade 1',       // Address
    'zip'                => '7400',             // Zip code
    'city'               => 'Herning',          // City
    'country'            => 'Denmark',          // Country
    'phone'              => '12345678',         // Phone
    'accept_sms'         => '1',				// "1" means accept SMS
    'phone_country_code' => '45',               // Phone country code
    'description'        => 'Lorem ipsum',      // Description
    'automated_mails'    => '1',		    	// "1" means automated mails
    '42'                 => '1.000,- DKK',      // Customfield "Budget" with customfield ID '42'
],
'stage_id' => 42,        // Stage ID
'owner_id' => null,      // Owner ID
'segment_id' => 13,      // Segment ID
'sync' => 1,             // "1" means that the Lead will get synced with 3rd party services, use "0" to disable for this Lead
'update' => 'all'        // for lead data updating: ["all" (default), "empty"] - "all" or "empty" TEXTUAL not-custom fields will be overridden by new values
```

Example response:
```
{
    "lead_key": "8001083940a98db179c0473d9bc75ccf"
}
```

### Lead
Endpoint: `https://www.leadscoreapp.dk/2.0/lead.json`

Accepts: `GET`

Requires a 'id', 'key' or 'email' to find a Lead


Request Examples
`https://www.leadscoreapp.dk/2.0/lead.json?email=name@example.com`

`https://www.leadscoreapp.dk/2.0/lead.json?id=1`

`https://www.leadscoreapp.dk/2.0/lead.json?key=01234567890abcdf`

Response Example:
```
{
    data : {
        'id': 1,                     //lead id 
        'email': 'name@example.com', // Email address
        'key':'01234567890abcdf',    // Lead's key
        'full_name': 'John Doe',     // Full name (Converts the name into first and last name)
        'first_name': 'John',        // First name
        'last_name': 'Doe',          // Last name
        'gender': 'MALE',            // Gender ('FEMALE', 'MALE' OR 'UNKNOWN')
        'title': 'Customer',         // Title
        'company': 'LeadScore App',  // Company
        'address': 'Bredgade 1',     // Address
        'zip': '7400',               // Zip code
        'city':'Herning',            // City
        'country':'Denmark',         // Country
        'phone':'12345678',          // Mobile Phone
        'phone_country_code': '45',  // Mobile Phone country code
        'landline':'+37477691681'    // Phone number
        'description':'Lorem ipsum', // Description
        '42': '1.000,- DKK',         // Customfield "Budget" with customfield ID '42'
    }
}
```


### Lead stage
Endpoint: `https://www.leadscoreapp.dk/2.0/lead/stage.json`

Accepts: `PUT`

Description: Change stage of one or more leads

Example request:
```
'leads' => [                                         // One or more leads with either ID, key or email
    [
        'id' => 42                                   // Lead ID
    ],
    [
        'key' => '8001083940a98db179c0473d9bc75ccf'  // Lead key
    ],
    [
        'email' => 'name@example.com'                // Lead email address
    ],
],
'stage_id' => 42,                                    // Stage ID
```

Example response:
```
{
    "leads": 3,
    "edited": 0,
    "same": 3
}
```

### Lead segment
Endpoint: `https://www.leadscoreapp.dk/2.0/lead/segment.json`

Accepts: `POST` and `DELETE`

Description: Add or remove segment from one or more leads

Example request:
```
'leads' => [                                         // One or more leads with either ID, key or email
    [
        'id' => 42                                   // Lead ID
    ],
    [
        'key' => '8001083940a98db179c0473d9bc75ccf'  // Lead key
    ],
    [
        'email' => 'name@example.com'                // Lead email address
    ],
],
'segment_id' => 42,                                  // Stage ID
```

Example response:
```
{
    "leads": 3,
    "changed": 0,
    "same": 3
}
```

### Lead note
Endpoint: `https://www.leadscoreapp.dk/2.0/lead/note.json`

Accepts: `POST`

Description: Add note to one or more leads

Example request:
```
'leads'  => [                                        // One or more leads with either ID, key or email
    [
        'id' => 42                                   // Lead ID
    ],
    [
        'key' => '8001083940a98db179c0473d9bc75ccf'  // Lead key
    ],
    [
        'email' => 'name@example.com'                // Lead email address
    ],
],
'note'  => 42,                                        // Note content (Up to 1024 characters),
'score' => 42,                                        // (Optional) Score to adjust lead by
'type'  => 'mail'                                     // (Optional) Note type ('mail', 'phone' or 'meeting')
```

Example response:
```
{
    "leads": 3,
    "notes_added": 3
}
```

### Lead deals
Endpoint: `https://www.leadscoreapp.dk/2.0/deals/leads.json`

Accepts: `POST`

Description: Add deal to one or more leads for one or more products

Example request:
```
'leads'  => [                                        // One or more leads with either ID, key or email
    [
        'id' => 42                                   // Lead ID
    ],
    [
        'key' => '8001083940a98db179c0473d9bc75ccf'  // Lead key
    ],
    [
        'email' => 'name@example.com'                // Lead email address
    ],
],
'user_id' => 42,                                     // User ID
'deal_products' => [                                 // One or more products with either ID or slug
    [
        'id' => 42                                   // Deal product ID
    ],
    [
        'slug' => 'lorem_ipsum'                      // Deal product slug
    ]
],
'sale_value' => 1337,                                // Deal value
'name' => 'Lorem ipsum',                             // Deal name
'note' => 'Lorem ipsum dolor sit amet'               // Deal note
'internal_note' => 'Lorem ipsum dolor sit amet',     // Internal note
'status_id' => 42,                                   // Deal status ID
'fields' => [										 // One or more deal custom field values
	42 => 'Foo',									 // Lead deal field ID => Value
	43 => 'Bar'
]
```

Example response:
```
{
    "new_lead_deals": [
        {
            "lead_deal_id": "1",
            "lead_id": "21",
            "deal_product_id": "11"
        },
        {
            "lead_deal_id": "2",
            "lead_id": "22",
            "deal_product_id": "12"
        }
    ],
    "added": 1,
    "skipped": {
        "leads": 0,
        "deal_products": 0
    }
}
```

### Deal products
Endpoint: `https://www.leadscoreapp.dk/2.0/deals/products.json`

Accepts: `POST`

Description: Add deal product

Example request:
```
'name'      => 'Lorem ipsum',                        // Deal product name
'date'      => '2016-06-24',                         // Deal product date
'slug'      => 'lorem_ipsum',                        // Deal product slug
'number'    => '42',                                 // Deal product number
'note'      => 'Lorem ipsum',                        // Deal product note
'deal_max'  => 42,                                   // Max deals per product
'status_id' => 42                                    // Deal product status ID
```

Example response:
```
{
    "deal_product_id": "42"
}
```

### Deal update
Endpoint: `https://www.leadscoreapp.dk/2.0/deal.json`

Accepts: `PUT`

Description: Update deal

Example request:
```
'id'            => 42,                               // (required) Deal ID
'user_id'       => 42,                               // User ID
'product_id'    => 42,                               // Deal product ID
'sale_value'    => 1337,                             // Deal value
'number'        => '42',                             // Deal number
'name'          => 'Lorem ipsum',                    // Deal name
'note'          => 'Lorem ipsum dolor sit amet'      // Deal note
'internal_note' => 'Lorem ipsum dolor sit amet',     // Internal note
'status_id'     => 42,                               // Deal status ID
'due_date'      => '1970-01-01',                     // Deal due date
```

Example response (updated deal):
```
{
    "deal": {
        "id": "42",
        "site_id": "1000000",
        "lead_id": "42",
        "user_id": "42",
        "last_updated_by_user_id": null,
        "product_id": "42",
        "name": "Lorem ipsum",
        "number": "42",
        "sale_value": "1337",
        "status": "Open",
        "deal_status_id": "42",
        "note": "Lorem ipsum dolor sit amet",
        "internal_note": "Lorem ipsum dolor sit amet",
        "due_date": "1970-01-01",
        "modified": "1970-01-01 00:00:00",
        "created": "1970-01-01 00:00:00"
    }
}
```

### Lead Summary
Endpoint: `https://www.leadscoreapp.dk/2.0/lead_summary.json?email={email}`

Accepts: `GET`

Description: Get lead's summary (settings & scores)

Example request:
```
Endpoint: `https://www.leadscoreapp.dk/2.0/lead_summary.json?email=test@leadscoreapp.dev`
```

Example response:
```
{
  "data": {
    "lead_id": "1",
    "email": "test@leadscoreapp.dev",
    "lsa_key": "8522904f954e8750a8fa1f222257b205",
    "accept_sms": false,
    "accept_auto_emails": true,
    "accept_newsletter": true,
    "leadscore_total": "123",
    "profile_winner": "Prfoile #2",
    "profile_last_engaged": "Prfoile #1",
    "lead_journey_stage": "Ny Lead",
    "website_last_visit_date": "2017-03-21 15:20:04",
    "segments": [
      "Segment #1"
    ],
    "profiles": [
      "Profile #1",
      "Prfoile #2"
    ]
  }
}
```

### Get All Profiles
Endpoint: `https://www.leadscoreapp.dk/2.0/profile.json`

Method: `GET`

Description: Returns all Site Profile

Example request:
```
GET https://www.leadscoreapp.dk/2.0/profile.json
```

Example response:
```json
{
  "data": [
    {
      "id": 42,
      "name": "Lorem ipsum",
      "slug": "lorem-ipsum",
      "sorting": 1,
      "threshold": 1,
      "is_integrated": 1,
      "update_field_name": 1,
      "update_field_value": "profile_name"
    },
    {
      "id": 43,
      "name": "Lorem ipsum",
      "slug": "lorem-ipsum",
      "sorting": 1,
      "threshold": 1,
      "is_integrated": 1,
      "update_field_name": 1,
      "update_field_value": "profile_name"
    }
  ]
}
```

### Get One Profile
Endpoint: `https://www.leadscoreapp.dk/2.0/profile.json?id={profile_id}`

Method: `GET`

Description: Returns one particular Profile

Example request:
```
GET https://www.leadscoreapp.dk/2.0/profile.json?id=42
```

Example response:
```json
{
  "data": {
    "id": 42,
    "name": "Lorem ipsum",
    "slug": "lorem-ipsum",
    "sorting": 1,
    "threshold": 1,
    "is_integrated": 1,
    "update_field_name": 1,
    "update_field_value": "profile_name"
  }
}
```

### Add Profile
Endpoint: `https://www.leadscoreapp.dk/2.0/profile.json`

Method: `POST`

Description: Creates Profile

Fields:
```
'name'                  => 'Lorem ipsum',  // Profile name
'threshold'             => 1,              // The minimum score before lead will be added to profile
'slug'                  => 'lorem-ipsum',  // Profile slug
'is_integrated'         => 1,              // When threshold is meet, then update field in MailChimp?
'update_field_name'     => 'FNAME',        // Field name in MailChimp
'update_field_value'    => 'profile_name', // Available values: `date`, 'profile_name', 'yes'
'rules' => [
    [
        'rule'          => 'lorem0',       // (regexp enabled) keys in URL
        'score_reward'  => 1,              // score to reward
    ],
    [
        'rule'          => 'lorem1',       // (regexp enabled) keys in URL
        'score_reward'  => 2,              // score to reward
    ]
]
```

Example request:
```
POST https://www.leadscoreapp.dk/2.0/profile.json

name=Lorem+ipsum&threshold=1&slug=lorem-ipsum&is_integrated=1&update_field_name=FNAME&update_field_value=profile_name&rules[0][rule]=lorem0&rules[0][score_reward]=2&rules[1][rule]=lorem1&rules[1][score_reward]=2
```

Example response:
```json
{
  "data": {
    "id": 42,
    "name": "Lorem ipsum",
    "slug": "lorem-ipsum",
    "sorting": 1,
    "threshold": 1,
    "is_integrated": 1,
    "update_field_name": 1,
    "update_field_value": "profile_name"
  }
}
```

### Add Profile in Batch
Endpoint: `https://www.leadscoreapp.dk/2.0/profile/batch.json`

Method: `POST`

Description: Creates Profile

Fields:
```
profiles => [
    [
        'name'                  => 'Lorem ipsum',  // Profile name
        'threshold'             => 1,              // The minimum score before lead will be added to profile
        'slug'                  => 'lorem-ipsum',  // Profile slug
        'is_integrated'         => 1,              // When threshold is meet, then update field in MailChimp?
        'update_field_name'     => 'FNAME',        // Field name in MailChimp
        'update_field_value'    => 'profile_name', // Available values: `date`, 'profile_name', 'yes'
        'rules' => [
            [
                'rule'          => 'lorem0',       // (regexp enabled) keys in URL
                'score_reward'  => 1,              // score to reward
            ],
            [
                'rule'          => 'lorem1',       // (regexp enabled) keys in URL
                'score_reward'  => 2,              // score to reward
            ]
        ]
    ]
]
```

Example request:
```
POST https://www.leadscoreapp.dk/2.0/profile/batch.json

profiles[0][name]=Lorem+ipsum+0&profiles[0][slug]=lorem-ipsum-0&profiles[0][threshold]=1&profiles[0][is_integrated]=1&profiles[0][update_field_name]=FNAME&profiles[0][update_field_value]=profile_name&profiles[0][rules][0][rule]=lorem00&profiles[0][rules][0][score_reward]=1&profiles[0][rules][1][rule]=lorem01&profiles[0][rules][1][score_reward]=2&profiles[1][name]=Lorem+ipsum+1&profiles[1][slug]=lorem-ipsum-1&profiles[1][threshold]=1&profiles[1][rules][0][rule]=lorem10&profiles[1][rules][0][score_reward]=1&profiles[1][rules][1][rule]=lorem11&profiles[1][rules][1][score_reward]=2
```

Example response:
```json
{
  "data": [
    {
      "id": "42",
      "name": "Lorem ipsum 0",
      "slug": "lorem-ipsum-0",
      "sorting": "0",
      "threshold": "1",
      "is_integrated": true,
      "update_field_name": "FNAME",
      "update_field_value": "profile_name"
    },
    {
      "id": "43",
      "name": "Lorem ipsum 1",
      "slug": "lorem-ipsum-1",
      "sorting": "1",
      "threshold": "1",
      "is_integrated": false,
      "update_field_name": null,
      "update_field_value": null
    }
  ]
}
```

### Update Profile
Endpoint: `https://www.leadscoreapp.dk/2.0/profile.json`

Method: `PUT`

Description: Updates Profile

Fields:
```
'id'                 => 42,             // Profile ID
'name'               => 'Lorem ipsum',  // Profile name
'threshold'          => 1,              // The minimum score before lead will be added to profile
'slug'               => 'lorem-ipsum',  // Profile slug
'is_integrated'      => 1,              // When threshold is meet, then update field in MailChimp?
'update_field_name'  => 'FNAME',        // Field name in MailChimp
'update_field_value' => 'profile_name', // Available values: `date`, 'profile_name', 'yes'
```

Example request:
```
PUT https://www.leadscoreapp.dk/2.0/profile.json

id=42&name=Lorem+ipsum&threshold=1&slug=lorem-ipsum&is_integrated=1&update_field_name=FNAME&update_field_value=profile_name
```

Example response:
```json
{
  "data": {
    "id": 42,
    "name": "Lorem ipsum",
    "slug": "lorem-ipsum",
    "sorting": 1,
    "threshold": 1,
    "is_integrated": 1,
    "update_field_name": 1,
    "update_field_value": "profile_name"
  }
}
```

### Delete Profile
Endpoint: `https://www.leadscoreapp.dk/2.0/profile.json`

Method: `DELETE`

Description: Deletes Profile

Fields:
```
'id' => 42 // Profile ID
```

Example request:
```
DELETE https://www.leadscoreapp.dk/2.0/profile.json

id=42
```

Example response:
```json
{
  "data": true
}
```

### Get All Profile's Rules
Endpoint: `https://www.leadscoreapp.dk/2.0/profile_rule.json?profile_id={profile_id}`

Method: `GET`

Description: Returns Profile's Rules

Example request:
```
GET https://www.leadscoreapp.dk/2.0/profile_rule.json?profile_id=42
```

Example response:
```json
{
  "data": [
    {
      "id": 42,
      "profile_id": 42,
      "rule": "lorem",
      "score_reward": 1
    },
    {
      "id": 43,
      "profile_id": 42,
      "rule": "lorem",
      "score_reward": 1
    }
  ]
}
```

### Get One Profile Rule
Endpoint: `https://www.leadscoreapp.dk/2.0/profile_rule.json?id={profile_rule_id}`

Method: `GET`

Description: Returns one particular Profile Rule

Example request:
```
GET https://www.leadscoreapp.dk/2.0/profile_rule.json?id=42
```

Example response:
```json
{
  "data": {
    "id": 42,
    "profile_id": 42,
    "rule": "lorem",
    "score_reward": 1
  }
}
```

### Add Profile Rule
Endpoint: `https://www.leadscoreapp.dk/2.0/profile_rule.json`

Method: `POST`

Description: Creates Profile Rule

Fields:
```
'profile_id'   => 42,      // Profile ID
'rule'         => 'lorem', // (regexp enabled) keys in URL
'score_reward' => 1        // score to reward
```

Example request:
```
POST https://www.leadscoreapp.dk/2.0/profile_rule.json

profile_id=42&rule=lorem&score_reward=1
```

Example response:
```json
{
  "data": {
    "id": 42,
    "profile_id": 42,
    "rule": "lorem",
    "score_reward": 1
  }
}
```

### Add Profile Rules in Batch
Endpoint: `https://www.leadscoreapp.dk/2.0/profile_rule/batch.json`

Method: `POST`

Description: Creates Profile Rules in Batch

Fields:
```
'profile_id'            => 42,      // Profile ID
'recreate`              => 1,       // If all profile's rules should be dropped first
'rules' => [
    [
        'rule'          => 'lorem', // (regexp enabled) keys in URL
        'score_reward'  => 1        // score to reward
    ]
]
```

Example request:
```
POST https://www.leadscoreapp.dk/2.0/profile_rule/batch.json

profile_id=42&recreate=1&rules[0][rule]=lorem0&rules[0][score_reward]=1&rules[1][rule]=lorem1&rules[1][score_reward]=2
```

Example response:
```json
{
  "data": [
    {
      "id": 42,
      "profile_id": 42,
      "rule": "lorem0",
      "score_reward": 1
    },
    {
      "id": 43,
      "profile_id": 42,
      "rule": "lorem1",
      "score_reward": 2
    }
  ]
}
```

### Update Profile Rule
Endpoint: `https://www.leadscoreapp.dk/2.0/profile_rule.json`

Method: `PUT`

Description: Updates Profile Rule

Fields:
```
'id'           => 42,      // Profile Rule ID
'rule'         => 'lorem', // (regexp enabled) keys in URL
'score_reward' => 1,       // score to reward
```

Example request:
```
PUT https://www.leadscoreapp.dk/2.0/profile_rule.json

id=42&rule=lorem&score_reward=1
```

Example response:
```json
{
  "data": {
    "id": 42,
    "profile_id": 42,
    "rule": "lorem",
    "score_reward": 1
  }
}
```

### Delete Profile Rule
Endpoint: `https://www.leadscoreapp.dk/2.0/profile_rule.json`

Method: `DELETE`

Description: Deletes Profile Rule

Fields:
```
'id' => 42 // Profile Rule ID
```

Example request:
```
DELETE https://www.leadscoreapp.dk/2.0/profile_rule.json

id=42
```

Example response:
```json
{
  "data": true
}
```
