# PHP Code Sample

## General
```php
<?php
$url = 'https://www.leadscoreapp.dk/2.0/<RESOURCE>.json';// set one of the endpoints

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

#curl_setopt($ch, CURLOPT_POSTFIELDS, [...]);// use if it is POST or PUT method

curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json',
    'Api-Key: <YOUR_API_KEY>',
]);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
var_dump(curl_exec($ch));
curl_close($ch);
```

## Get stages

```php
<?php
$url = 'https://www.leadscoreapp.dk/2.0/stages.json';

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Api-Key: <YOUR_API_KEY>',
]);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
curl_close($ch);

var_dump($result['stages']);
```

## Create/update lead

```php
<?php
$url = 'https://www.leadscoreapp.dk/2.0/lead.json';

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
    'fields' => [
        'email'              => 'name@example.test',// Email address
        'full_name'          => 'John Doe',         // Full name (Converts the name into first and last name)
        'first_name'         => 'John',             // First name
        'last_name'          => 'Doe',              // Last name
        'gender'             => 'MALE',             // Gender ('FEMALE', 'MALE' OR 'UNKNOWN')
        'title'              => 'Customer',         // Title
        'company'            => 'Company',          // Company
        'address'            => 'Address',          // Address
        'zip'                => '7400',             // Zip code
        'city'               => 'Herning',          // City
        'country'            => 'Denmark',          // Country
        'phone'              => '12345678',         // Phone
        'accept_sms'         => '1',                // "1" means accept SMS
        'phone_country_code' => '45',               // Phone country code
        'description'        => 'Lorem ipsum',      // Description
        'automated_mails'    => '1',                // "1" means automated mails
        '42'                 => '1.000,- DKK'       // Customfield "Budget" with customfield ID '42'
    ],
    'stage_id' => null,      // Stage ID
    'owner_id' => null,      // Owner ID
    'segment_id' => null,    // Segment ID
    'sync' => 1,             // "1" means that the Lead will get synced with 3rd party services, use "0" to disable for this Lead
    'update' => 'all',       // for lead data updating: ["all" (default), "empty"] - "all" or "empty" TEXTUAL not-custom fields will be overridden by new values
]));

curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Api-Key: <YOUR_API_KEY>',
]);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
curl_close($ch);

var_dump($result['lead_key']);
```
