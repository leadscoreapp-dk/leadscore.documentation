# LeadScore App JavaScript SDK Documentation

Welcome to the LeadScore App JavaScript SDK documentation.

## Getting Started

Getting started with Sentry is a three step process:

1. Get JS API Key
2. Install JS SDK
3. Configure SDK

## Get JS API KEY

You will need to go to [Sites Setup](https://leadscoreapp.dk/lead_score/setup#sites) page in your account. Then click *Advanced Option* in bottom of the page, enable **Enable API for JavaScript SDK** and copy **API Key**.

## Install an SDK

The quickest way to get started is to put this HTML code into your page:

```html
<script src="https://leadscoreapp.dk/lead_score/js/sdk/v1.js"></script>
```

## Configure the SDK

You should initialize the `LeadScoreApp` instance preferably at the end of your page load using your Site ID and JS API Key:
```html
<script>
    var LSA = new LeadScoreApp(<SITE_ID>, '<JS_API_KEY>');
</script>
```

After that you can use the next methods:


`LSA.saveLead(<object>data, <function>callback = null)` - allows to create/update lead. Data could of the next format:

```json
{
  "fields": {
    "email": "john.doe@example.com",
    "key": "abcd123",
    "full_name": "John Doe",
    "first_name": "John",
    "last_name": "Doe",
    "gender": "MALE",
    "title": "Customer",
    "company": "LeadScore App",
    "address": "Bredgade 1",
    "zip": "7400",
    "city": "Herning",
    "country": "Denmark",
    "phone": "12345678",
    "accept_sms": "1",
    "phone_country_code": "45",
    "description": "Lorem ipsum",
    "automated_mails": "1",
    "42": "1.000,- DKK"
  },
  "stage_id": 42,
  "owner_id": 42,
  "segment_id": 42,
  "sync": 1
}
```

By setting `callback` you can check the result of lead saving - your function will get lead's key as a argument:

```javascript
LSA.saveLead({ "fields": { "key": "abcd123" }}, function (leadKey) {
    console.log(leadKey);
});
```


`LSA.getLeadKey()` - finds current lead's key either from URL or cookie and returns it.


`LSA.generateLeadKey(<function>callback)` - generates new lead key. By setting `callback` you can check the result of key generation - your function will get 2 argument: new lead key and a flag if anonymous lead tracking is enabled in your account: 

```javascript
LSA.generateLeadKey(function (leadKey, isAnonTrackingEnabled) { 
    console.log(leadKey, isAnonTrackingEnabled);
});
```


`LSA.trackPageview(<string>url = null)` - allows to track page's view. You can set `url` to be tracked as viewed or leave it empty to track current page's view.
